create table recipes (
       recipe_id integer not null unique,
       name varchar(30) not null unique,
       instructions varchar(100),
       image varchar(100),
       primary key(recipe_id));

create table ingredients (
       ing_id integer,
       name varchar(50),
       size varchar(30),
       primary key(ing_id));
--       UNIQUE(name, size));

create table liquor (
       liq_id integer,
       liquor_name varchar(100),
       proof real,
       liq_type varchar (100),
       liq_price real,
       foreign key(liq_id) references ingredients(ing_id));

create table garnish (
       garnish_type varchar (30) not null unique);
--       foreign key(garnish_type) references ingredients(ing_id));

create table mixers (
       mix_type varchar (30) not null);
--       foreign key(mix_type) references ingredients(ing_id));
